package Modelo;

/**
 *
 * @author sjimenez
 */
public class Calcular {
    
    private double capital;
    private double interes;
    private double years;
    private double resultado;
    
    public Calcular(){
        
        this.capital=0;
        this.interes=0;
        this.years=0;
        this.resultado=0;
    }
    
    public void Resultado(){
        this.setResultado(((this.capital*this.interes)/100)*this.years);
    }

    /**
     * @return the capital
     */
    public double getCapital() {
        return capital;
    }

    /**
     * @param capital the capital to set
     */
    public void setCapital(double capital) {
        this.capital = capital;
    }

    /**
     * @return the interes
     */
    public double getInteres() {
        return interes;
    }

    /**
     * @param interes the interes to set
     */
    public void setInteres(double interes) {
        this.interes = interes;
    }

    /**
     * @return the years
     */
    public double getYears() {
        return years;
    }

    /**
     * @param years the years to set
     */
    public void setYears(double years) {
        this.years = years;
    }

    /**
     * @return the resultado
     */
    public double getResultado() {
        return resultado;
    }

    /**
     * @param resultado the resultado to set
     */
    public void setResultado(double resultado) {
        this.resultado = resultado;
    }
    
}
