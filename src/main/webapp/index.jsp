<%-- 
    Document   : index
    Created on : 05-04-2020, 20:03:22
    Author     : sjimenez
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Prueba Unidad 01</title>
    </head>
    <body>
        <h1>Control de Intereses</h1>
        <form action="Controlador" method="POST">
            <table>
                <tr>
                    <td>Capital</td><td><input type="text" placeholder="0" name="capital" /></td>
                </tr>
                <tr>
                    <td>Tasa Anual</td><td><input type="text" placeholder="0" name="interes" /></td>
                </tr>
                <tr>
                    <td>Año</td><td><input type="text" placeholder="0" name="years" /></td>
                </tr>
                <tr>
                    <td><button type="submit">Calcular</button></td>
                </tr>
            </table>
        </form>
    </body>
</html>
