<%-- 
    Document   : salida
    Created on : 05-04-2020, 20:23:22
    Author     : sjimenez
--%>

<%@page import="Modelo.Calcular"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Calculo de Interes</h1>
            <%
                Calcular obj=new Calcular();
                obj=(Calcular)request.getAttribute("ObjetoJava");
                if (obj !=null){
                    out.print("Un capital de $");
                    out.print(obj.getCapital());
                    out.print(", con una tasa anual de ");
                    out.print(obj.getInteres());
                    out.print("%, a ");
                    out.print(obj.getYears());
                    out.print(" años, genera un interes de ");
                    out.print(obj.getResultado());
                }
            %>   
    </body>
</html>
